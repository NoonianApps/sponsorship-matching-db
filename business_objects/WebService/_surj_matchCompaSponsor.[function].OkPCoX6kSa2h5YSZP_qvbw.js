function(db, queryParams, _, Q) {
    const compaId = queryParams.compa;
    const sponsorId = queryParams.sponsor;
    
    
    if(!compaId || !sponsorId) {
        throw new Error('missing required param');
    }
    
    const moment = require('moment');
    
    return Q.all([
        db.Compa.findOne({_id:compaId}),
        db.Sponsor.findOne({_id:sponsorId})
    ])
    .spread((c, s)=>{
        
        if(!c || !s) {
            throw new Error('invalid compa or sponsor id: '+compaId+' / '+sponsorId);
        }
        
        let m = new db.SponsorCompaMatch({
            start_date:moment().format('YYYY-MM-DD'),
            is_current:true,
            sponsor:{_id:sponsorId},
            compa:{_id:compaId}
        });
        
        c.status = 'matched';
        s.status = 'matched';
        
        
        return Q.all([m.save(), c.save(), s.save()]);
    }).spread((m, c, s)=>{
        return {result:'success', message:'Successfully matched '+c._disp+' and '+s._disp};
    })
    ;
    
}