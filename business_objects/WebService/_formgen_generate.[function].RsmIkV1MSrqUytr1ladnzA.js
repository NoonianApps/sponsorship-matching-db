function(queryParams, i18n, db, _, Q) {
    const specId = queryParams.id;
    
    if(!specId) {
        throw new Error('missing required params');
    }
    
    const getEnumerationValues = function(bodId) {
        const tdMap = db[bodId]._bo_meta_data.type_desc_map;
        
        const enumFields = {};
        const enumList = [];
        const lgKeys = [];
        _.forEach(tdMap, (td, fieldName) => {
            td = td instanceof Array ? td[0] : td;
            if(td.type === 'enum') {
                enumFields[fieldName] = td.enum;
                enumList.push(td.enum);
                lgKeys.push('db.enum.'+td.enum);
            }
        });
        
        if(!enumList) {
            return Q({});
        }
        
        return Q.all([
            db.LabelGroup.find({key:{$in:lgKeys}}).lean().exec(),
            db.Enumeration.find({name:{$in:enumList}}).lean().exec()
        ])
        .spread(function(lgList, enumList) {
            const lgMap = _.indexBy(lgList, 'key');
            const enumMap = _.indexBy(enumList, 'name');
            
            const ret = {};
            
            _.forEach(enumFields, (enumName, fieldName)=>{
                let vals = ret[fieldName] = [];
                let enumeration = enumMap[enumName] || {};
                
                let lgkey = 'db.enum.'+enumName;
                let lg = lgMap[lgkey] && lgMap[lgkey].value || {};
                
                _.forEach(enumeration.values, val=>{
                    vals.push({
                        value: val,
                        label: (lg[val] || val)
                    });
                });
            });
            return ret;
            
        });
    };
    
    return db.FormGenSpec.findOne({_id:specId}).then(function(fgSpec) {
        
        const formgenId = fgSpec.template && fgSpec.template._id;
        const fgConfig = fgSpec.config;
        const bodId = fgSpec.business_object_class && fgSpec.business_object_class._id;
        
        if(!formgenId) {
            throw new Error('FormGenTemplate required!')
        }
        if(!bodId) {
            throw new Error('BusinessObject Class required!');
        }
        if(!fgConfig || !fgConfig.fields || !(fgConfig.fields instanceof Array)) {
            throw new Error('Bad config!');
        }
        
        return Q.all([
            db.FormGenTemplate.findOne({_id:formgenId}),
            db.FormGenFieldTemplate.find({template:formgenId}),
            getEnumerationValues(bodId),
            i18n.getBoLabelGroup(db[bodId]._bo_meta_data.class_name)
        ])
        .spread(function(fgTemplate, fgFieldTemplateList, enumValues, boLabels) {
            if(!fgTemplate) {
                throw new Error('invalid FormGenTemplate id');
            }
            
            
            if(fgConfig.labels) {
                _.assign(boLabels, fgConfig.labels);
                
                //Copy over enumeration labels
                _.forEach(fgConfig.labels, (labelObj, fieldName) => {
                    if(fieldName.indexOf('#') === 0) {
                        let myVals = enumValues[fieldName.substring(1)];
                        if(myVals) {
                            //overwrite labels from LabelGroup
                            myVals.forEach(entry=>{
                                if(labelObj[entry.value]) {
                                    entry.label = labelObj[entry.value];
                                }
                            })
                        }
                    }
                });
                    
                
            }
            
            const templateMap = {};
            const arrTemplateMap = {};
            const keyTemplateMap = {};
            _.forEach(fgFieldTemplateList, ft=>{
                let m = ft.is_array ? arrTemplateMap : templateMap;
                
                _.forEach(ft.field_types, ftRef => {
                    m[ftRef._disp] =ft.html;
                });
                
                if(ft.key) {
                    keyTemplateMap[ft.key] = ft.html;
                }
            });
            
            
            const tdMap = db[bodId]._bo_meta_data.type_desc_map;
            
            //build up fieldListHtml
            const fieldOpts = fgConfig.field_opts  = fgConfig.field_opts || {};
            var fieldListHtml = '';
            
            fgConfig.fields.forEach(fieldDef=>{
                var fieldName, templateKey;
                if(typeof fieldDef === 'string') {
                    fieldName = fieldDef;
                }
                else {
                    fieldName = fieldDef.field || '';
                    templateKey = fieldDef.template;
                }
                
                var td = tdMap[fieldName];
                
                var fieldTemplateHtml;
                
                if(templateKey) {
                    fieldTemplateHtml = keyTemplateMap[templateKey]; 
                }
                
                if(td && !fieldTemplateHtml) {
                    let isArray = td instanceof Array;
                    td = isArray ? td[0] : td;
                    let m = isArray ? arrTemplateMap : templateMap;
                    fieldTemplateHtml = m[td.type];
                    if(!fieldTemplateHtml) {
                        throw new Error(fieldName+' missing template for field type '+td.type);
                    }
                }
                else if(!fieldTemplateHtml) {
                    console.error('SKIPPING Invalid field specifier: %j', fieldDef);
                    return;
                }
                
                let templateFn = _.template(fieldTemplateHtml);
                let ctx = {
                    fieldDef,
                    fieldName,
                    fieldLabel:boLabels[fieldName],
                    enumOptions:enumValues[fieldName],
                    opts:(fieldOpts[fieldName] || {})
                };
                
                fieldListHtml += templateFn(ctx)+'\n';
                
            });
            
            fgConfig.fieldListHtml = fieldListHtml;
            
            let mainTemplateFn = _.template(fgTemplate.html_template);
            
            console.log('Compiling template with config: %j', fgConfig);
            
            let fullHtml = mainTemplateFn(fgConfig);
            
            let newResource = new db.HtmlResource({
                path:fgConfig.target_path || 'generated_forms',
                name:fgConfig.target_name || fgSpec.business_object_class._disp+'.html',
                content:fullHtml
            });
            
            return newResource.save().then(function() {
                return {
                    action:{
                        state:'dbui.edit',
                        params:{className:'HtmlResource',id:newResource._id}
                    }
                }
            });
        });
    });
    
    
}