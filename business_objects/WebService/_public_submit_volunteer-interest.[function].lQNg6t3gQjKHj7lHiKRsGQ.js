function(db, req, res, postBody) {
    const moment = require('moment');
    res.type('html');
    return db.FormGenSpec.findOne({_id:'GbwLM_k5Qsa7i526fnec3w'}).then(fgs=>{
        return fgs.convertFormPost(postBody);
        
    })
    .then(newBo=>{
        
        newBo.source = req.ip;
        newBo.submit_ts = moment().format();
        newBo.status = 'new';
        newBo.info_source = 'website';
        
        return newBo.save();
    })
    .then(()=>{
        return db.HtmlResource.findOne({_id:'PMfXqrWqTSut1uQPpVaPhA'});
    })
    .then(hr=>{
        return hr.content;
    })
    ;
}