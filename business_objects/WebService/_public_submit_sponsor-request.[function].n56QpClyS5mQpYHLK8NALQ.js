function(db, req, res, postBody) {
    const moment = require('moment');
    res.type('html');
    return db.FormGenSpec.findOne({_id:'2G3cdoNPTGi8r9PXDiHR8g'}).then(fgs=>{
        return fgs.convertFormPost(postBody);
        
    })
    .then(newBo=>{
        newBo.source = req.ip;
        newBo.submit_ts = moment().format();
        newBo.status = 'new';
        return newBo.save();
    })
    .then(()=>{
        return db.HtmlResource.findOne({_id:'PMfXqrWqTSut1uQPpVaPhA'});
    })
    .then(hr=>{
        return hr.content;
    })
    ;
}