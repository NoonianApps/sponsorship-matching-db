function(db, queryParams, _, Q) {
    const srfId = queryParams.id;
    
    if(!srfId) {
        throw new Error('missing required param');
    }
    
    
    return Q.all([
        db.SponsorshipRequestForm.findOne({_id:srfId}),
        db.Compa.findOne({submission_form:srfId})
    ])
    .spread((srf, existingCompa)=>{
        
        if(existingCompa) {
            return Q.all([srf, existingCompa]);
        }
        
        if(!srf) {
            throw new Error('invalid SponsorshipRequestForm id: '+srfId);
        }
        srf.status = 'accepted';
        
        const compa = new db.Compa({
            submission_form:{_id:srfId},
            status:'seeking'
        });
        
        const formTdm = srf._bo_meta_data.type_desc_map;
        const compaTdm = compa._bo_meta_data.type_desc_map;
        
        _.forEach(compaTdm, (td, f)=>{
            if(f.indexOf('_') === 0 || f==='status') {
                return;
            }
            if(formTdm[f]) {
                compa[f] = srf[f];
            }
            for(let i=2; i <=3; i++) {
                let fAlt = f+'_'+i;
                if(formTdm[fAlt]) {
                    compa[f] = compa[f] || srf[fAlt];
                }
            }
        });
        
        return Q.all([srf.save(), compa.save()]);
    }).
    spread((srf, compa)=>{
        return {
            action:{
                state:'dbui.pivot_page',
                params:{
                    className:'Compa',
                    id:compa._id,
                    perspective:'seeking'
                }
            }
        }
    })
    
}