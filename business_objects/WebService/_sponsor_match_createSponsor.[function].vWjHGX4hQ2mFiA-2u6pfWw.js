function(db, queryParams, _, Q) {
    const vifId = queryParams.id;
    
    if(!vifId) {
        throw new Error('missing required param');
    }
    
    
    return Q.all([
        db.VolunteerInterestForm.findOne({_id:vifId}),
        db.Sponsor.findOne({submission_form:vifId})
    ])
    .spread((vif, existingSponsor)=>{
        
        if(existingSponsor) {
            return Q.all([vif, existingSponsor]);
        }
        
        if(!vif) {
            throw new Error('invalid VolunteerInterestForm id: '+vifId);
        }
        vif.status = 'accepted';
        
        const sponsor = new db.Sponsor({
            submission_form:{_id:vifId},
            status:'screened',
            financial_support_duration:vif.financial_duration,
            languages:(vif.spanish_speaker ? 'Spanish' : '')
        });
        
        const sponsorTdm = sponsor._bo_meta_data.type_desc_map;
        _.forEach(vif._bo_meta_data.type_desc_map, (td, f)=>{
            if(f.indexOf('_') === 0 || f==='status') {
                return;
            }
            if(sponsorTdm[f]) {
                sponsor[f] = vif[f];
            }
        });
        
        return Q.all([vif.save(), sponsor.save()]);
    }).
    spread((vif, sponsor)=>{
        return {
            action:{
                state:'dbui.pivot_page',
                params:{
                    className:'Sponsor',
                    id:sponsor._id,
                    perspective:'screened'
                }
            }
        }
    })
    
}