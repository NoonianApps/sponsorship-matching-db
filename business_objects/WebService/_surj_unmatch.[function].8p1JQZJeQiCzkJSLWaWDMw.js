function(db, queryParams, _, Q) {
    
    const matchId = queryParams.id;
    
    if(!matchId) {
        throw new Error('missing required param');
    }
    
    const moment = require('moment');
    
    return db.SponsorCompaMatch.findOne({_id:matchId}).then(m=>{
        
        return Q.all([
            db.Compa.findOne({_id:m.compa._id}),
            db.Sponsor.findOne({_id:m.sponsor._id})
        ])
        .spread((c, s)=>{
            
            m.end_date = moment().format('YYYY-MM-DD');
            m.is_current = false;
            
            let promiseArr = [m.save()];
            
            if(c.status === 'matched') {
                c.status = 'seeking';
                promiseArr.push(c.save());    
            }
            if(s.status === 'matched') {
                s.status = 'vetted_2';
                promiseArr.push(s.save());    
            }
            
            
            return Q.all(promiseArr).then(()=>{
                return {result:'success', message:'Successfully unmatched '+c._disp+' and '+s._disp};
            });
        })
    });
    
}