function(db, queryParams, _, Q) {
    const srfId = queryParams.id;
    
    
    if(!srfId) {
        throw new Error('missing required param');
    }
    
    return db.SponsorshipRequestForm.findOne({_id:srfId}).then(srf=>{
        if(srf) {
            srf.status = 'rejected';
            return srf.save();
        }
    })
    .then(()=>{
        return {
            action:{
                state:'dbui.list',
                params:{
                    className:'SponsorshipRequestForm',
                    perspective:'default'
                }
            }
        }
    })
    ;
    
    
}