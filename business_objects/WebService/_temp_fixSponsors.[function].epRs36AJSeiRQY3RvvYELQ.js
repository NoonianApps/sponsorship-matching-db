function(db, _, Q) {
    
   return db.LabelGroup.find({key:{$regex:'^app.surj'}}).then(mList=>{
       let rex = /^app.surj.(.+)/;
       let ret=[];
       _.forEach(mList, m=>{
           let suf = rex.exec(m.key);
           m.key = 'app.sponsorship_match.'+suf[1];
           m.save();
           ret.push(suf[1]);
       })
       return ret;
   })
}