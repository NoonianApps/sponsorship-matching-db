function ($scope, $q, Dbui, DbuiFieldType, db) {
    
   
   const formClass = {
       Sponsor:'VolunteerInterestForm',
       Compa:'SponsorshipRequestForm'
   }
    
    $scope.initPromise.then(function() {
        let perspective = $scope.myTab.perspective || $scope.perspectiveName;
        
        let centralObj = $scope.centralObj;
        
        if(centralObj.submission_form && centralObj.submission_form._id) {
            let myFormClass = formClass[$scope.centralClass];
            
            return $q.all([
                Dbui.getPerspective(perspective, myFormClass, 'view'),
                db[myFormClass].findOne({_id:centralObj.submission_form._id}),
                DbuiFieldType.cacheTypeInfoForClass(myFormClass, 'view')
            ]).then(function(resultArr) {
                $scope.viewPerspective = resultArr[0];
                $scope.formObj = resultArr[1];
                if(!$scope.formObj) {
                    $scope.myTab.isHidden = true;
                }
            });
            
        }
        else {
            $scope.myTab.isHidden = true;
        }
        
        
    })
    
    ;
}