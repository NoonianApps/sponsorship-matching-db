function ($scope,$anchorScroll, $q, Dbui, DbuiFieldType,DbuiAlert) {
    
    //Wire up tab object's formStatus for consumption by support_portal's tab heading.
    // $scope.myTab.formStatus = $scope.detainee.formStatus;
    
    const resetFormStatus = function() {
        $scope.centralObj.formStatus = {isDirty:false};
    };
    
    $scope.$watch('centralObj.formStatus', function() {
        $scope.myTab.formStatus = $scope.centralObj.formStatus;
    });
    
    $scope.save = function() {
        // console.log('SAVING SAVING SAVING', $scope.detainee);
        $scope.myTab.waiting = true;
        $scope.centralObj.save().then(function() {
            resetFormStatus();
            $scope.myTab.waiting = false;
            $anchorScroll('top');
        },
        function(err) {
            console.error(err);
            DbuiAlert.danger(err);
        }
        );
    };
    
    $scope.initPromise.then(function(detainee) {
        let perspective = $scope.myTab.perspective || $scope.perspectiveName;
        return $q.all([
            Dbui.getPerspective(perspective, $scope.centralClass, 'edit'),
            DbuiFieldType.cacheTypeInfoForClass($scope.centralClass, 'edit')
        ]);
    })
    .then(function(resultArr) {
        $scope.editPerspective = resultArr[0];
    })
    .then(function() {
        resetFormStatus();
    })
    ;
}