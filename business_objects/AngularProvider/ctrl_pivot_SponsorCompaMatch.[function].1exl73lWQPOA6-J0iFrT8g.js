function ($scope, db, DbuiObjectPicker, NoonWebService, DbuiAlert, $state) {
    
   
   const fieldClassMap = {
       Sponsor:'sponsor',
       Compa:'compa',
       compa:'Compa',
       sponsor:'Sponsor'
   };
   const oppField = {
       compa:'sponsor',
       sponsor:'compa'
   };
   
   const allowableStatus = {
       Sponsor:'vetted_2',
       Compa:'seeking'
   };
    
    $scope.initPromise.then(function() {
        
        let myClass = $scope.centralClass;
        let centralObj = $scope.centralObj;
        
        $scope.allowMatch = (centralObj.status === allowableStatus[myClass]);
        
        
        let myRefField = fieldClassMap[myClass];
        
        let partnerField = $scope.partnerField = oppField[myRefField];
        let partnerClass = fieldClassMap[$scope.partnerField];
        
        $scope.partnerType = (partnerClass === 'Compa' ? 'Client' : partnerClass);
        
        let query = {};
        query[myRefField] = centralObj._id;
        
        const refreshList = function() {
            
            let matchList = $scope.matchList = db.SponsorCompaMatch.find(query, null, {sort:{start_date:1}});
            
            matchList.$promise.then(function(){
                let lm = $scope.latestMatch = matchList.length && matchList[matchList.length-1];
                
                if(lm) {
                    $scope.latestMatchRef = lm[partnerField];
                    $scope.allowMatch = (centralObj.status === allowableStatus[myClass]) && !lm.is_current;
                }
                
            });
        };
        
        refreshList();
        
        const matchSelected = function(ref) {
            
            let params = {};
            params[myRefField] = centralObj._id;
            params[partnerField] = ref._id;
            NoonWebService.call('surj/matchCompaSponsor', params).then(function(resp) {
                refreshList();
                $scope.refreshCentralObj();
                if(resp.message) {
                    DbuiAlert.success(resp.message);
                }
                if(resp.error) {
                    DbuiAlert.danger(resp.error);
                }
            },
            function(err) {
                DbuiAlert.danger(err);
            })
        } 
        
        $scope.match = function() {
            DbuiObjectPicker.showPickerDialog(partnerClass, 'match_pick', true, matchSelected);
        };
        
        $scope.unmatch = function() {
            if(confirm('Are you sure you want to unmatch?')) {
                NoonWebService.call('surj/unmatch', {id:$scope.latestMatch._id}).then(function(resp) {
                    refreshList();
                    $scope.refreshCentralObj();
                    if(resp.message) {
                        DbuiAlert.success(resp.message);
                    }
                    if(resp.error) {
                        DbuiAlert.danger(resp.error);
                    }
                },
                function(err) {
                    DbuiAlert.danger(err);
                })
            }
        };
        
        $scope.selectPartner = function(m) {
            
            $state.go('dbui.pivot_page', {
                className:partnerClass,
                id:m[partnerField]._id,
                perspective:'default'
            });
        };
        $scope.saveNote = function(m) {
            m.save().then(function() {
                m.editingNote = false;
            })
        };
    })
    ;
}