function ($scope, db, Dbui, DbuiFieldType, $timeout) {
    
    db.EmailRecipientList.findOne({_id:'KXiEn_C9TOOWoD7Rv8xTjg'}).$promise.then(vir=>{
        $scope.virStatus = {};
        $scope.volunteerInterestRecipients = vir;
        $timeout(()=>{
            $scope.virStatus.isDirty = false;
        }, 200);
        // $scope.$watchCollection('volunteerInterestRecipients.addresses', ()=>{
        //     console.log($scope.virStatus)
        // });
    });
    $scope.saveVir = function() {
        $scope.volunteerInterestRecipients.save().then(()=>{
            $scope.virStatus = {};
        });
    };
    
    $scope.emailRecipientsPerspective = {
        layout:Dbui.normalizeLayout(['addresses'])
    };
    
    $scope.messageEditPerspective = {
        layout:Dbui.normalizeLayout(["subject","html_body", "attachments"]),
        fieldCustomizations: {
          html_body: {
            uiSpec: 'dbui.addon.tinymce_editor'
          }
        }
    };
    
    
    $scope.weeklyReportRecipients = [
        {title:'Organize', listId:'wVhx_bDHQtu1O29E9W8BdA'},
        {title:'Faith', listId:'MC9IKiimTNmFKPE7qU3-7A'},
        {title:'Sponsor', listId:'zuHMogPwQMSlvabABnocBg'},
        {title:'Financial', listId:'Ufex4aElTWuHhK871r-vtg'},
    ];
    
    
    const welcomeMessage = 'HQ7V5EnTTN60ZdCwWKhiCg';
    
    
    $scope.editRecpientList = function(wm) {
        if($scope.editingList) {
            $scope.editingList.bo.save();
            $scope.editingList = null;
        }
        if(!wm.bo) {
            wm.bo = db.EmailRecipientList.findOne({_id:wm.listId});
        }
        wm.bo.$promise.then((msg)=>{
            $timeout(()=>{ $scope.editingList = wm; });
        });
    };
    
    $scope.editWelcomeMessage = function() {
        if(!$scope.editingWelcomeMessage) {
            
            db.EmailMessage.findOne({_id:welcomeMessage}).$promise.then(msg=>{
                $scope.editingWelcomeMessage = msg;
            });
        }
    };
    
    $scope.saveWelcomeMessage = function() {
        
        $scope.editingWelcomeMessage.save().then(()=>{
            $scope.editingWelcomeMessage = null;
        });
        
    };
    
    DbuiFieldType.cacheTypeInfoForClass('EmailMessage');
    
    
}