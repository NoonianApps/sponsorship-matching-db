function (db, EmailService, Q, _, i18n) {
    const moment = require('moment');
    const exports = {};
    
    const recipientLists = {
        organize:'wVhx_bDHQtu1O29E9W8BdA',
        faith:'MC9IKiimTNmFKPE7qU3-7A',
        host:'zuHMogPwQMSlvabABnocBg',
        financial:'Ufex4aElTWuHhK871r-vtg'
    };
    
    exports.sendWeeklyReports = function() {
        
        const lastWeek = moment().subtract(1, 'week');
        
        var labels;
        
        const emailTransport='Y9InlBNKTDCDpVdMfRGQOA';
        const messageId='CkXyEwujROK2sXJAgZIhtw';
        
        
        const sendEmail = function(via, supList) {
            
            return db.EmailRecipientList.findOne({_id:recipientLists[via]}).lean().exec().then(erl=>{
                if(!erl || !erl.addresses || !erl.addresses.length) {
                    return;
                }
                // console.log(JSON.stringify(supList, null, 3));
                _.forEach(supList, sup=>{
                    let viList = [];
                    _.forEach(sup.volunteering_interests, vi=>{
                        viList.push(labels.VolunteerInterest[vi]);
                    });
                    sup.volunteering_interests = viList;
                    sup.submit_ts = moment(sup.submit_ts).format('M/D/YY');
                    sup.housing_duration = labels.HousingDuration[sup.housing_duration];
                    sup.individual_family_open = labels.IndividualOrFamily[sup.individual_family_open];
                });
                
                let context = {
                    startDate:lastWeek.format('M/D/YY'),
                    endDate:moment().format('M/D/YY'),
                    supList, 
                    via:labels.VolunteerInterest[via]
                };
                
                return EmailService.sendEmail(emailTransport, messageId, context, erl.addresses).then(
                    function(info) {
                        console.log('Successfully sent VolunteerInterestForm notification message: %j', info);
                    },
                    function(err) {
                        console.log('ERROR SENDING VolunteerInterestForm notification message %j', err);
                    }
                );
            });
        };
        
        
        return Q.all([
            i18n.getLabelGroup('db.enum.VolunteerInterest'),
            i18n.getLabelGroup('db.enum.IndividualOrFamily'),
            i18n.getLabelGroup('db.enum.HousingDuration')
        ])
        .then(function([viLabels, ifLabels, hdLabels]) {
            
            labels = {
                VolunteerInterest:viLabels,
                IndividualOrFamily:ifLabels,
                HousingDuration:hdLabels
            };
            
            
            const areas = Object.keys(recipientLists);
            
            
            const dateRange = {
                submit_ts:{$gt:lastWeek.format('YYYY-MM-DD')}
            };
            
            const queryArr = [];
            
            areas.forEach(via=>{
                queryArr.push(
                    db.VolunteerInterestForm.find({
                        $and:[
                            dateRange,
                            {volunteering_interests:via}
                        ]
                    }).sort({submit_ts:1}).lean().exec()
                );
            });
            
            return Q.all(queryArr).then(resultArr=>{
                
                var promiseChain = Q(true);
                
                for(let i=0; i < areas.length; i++) {
                    let supList = resultArr[i];
                    let via = areas[i];
                    
                    if(supList.length) {
                        promiseChain = promiseChain.then(sendEmail.bind(null, areas[i], supList));
                    }
                    
                }
                
                return promiseChain.then(()=>{return {result:'success'}});
            });
        });
    }
    
    return exports;
}