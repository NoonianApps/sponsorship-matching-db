function (db, _) {
    
    if(this.city) {
        this.city = _.startCase(this.city.trim().toLowerCase());
    }
    
    if(this.zip && !(this.city && this.state)) {
        const THIS = this;
        return db.ZipCode.findOne({zip:this.zip}).then(function(zc) {
            if(zc) {
                THIS.city = zc.city;
                THIS.state = zc.state;
            }
        });
    }
}