function() {
    if(!this.submit_ts) {
        const moment = require('moment');
        this.submit_ts = moment().format();
    }
}