function(db) {
    if(!this.submit_ts) {
        const moment = require('moment');
        this.submit_ts = moment().format();
    }
    if(this.zip && !(this.city && this.state)) {
        return db.ZipCode.findOne({zip:this.zip}).then(zc=> {
            if(zc) {
                this.city = zc.city;
                this.state = zc.state;
            }
        });
    }
}